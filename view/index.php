<!DOCTYPE html>
<html lang="en">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">

</html>
<?php
    // echo "path: " , dirname(dirname(__DIR__));
    define("ROOT_PATH" , dirname(__DIR__));

    if(isset($_GET['controller'])){
        $controller = $_GET['controller'];
    }else{
        $controller = '';
    }
    
    switch($controller){
        case 'user':
            require_once('controller/user/index.php');
        case 'login':
            require_once('controller/login/index.php');
        case 'uploadfile':
            require_once('controller/uploadfile/index.php');
    }

?>